<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use redirect;

class CastController extends Controller
{
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('cast', compact('casts'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        $query = DB::table('casts')->insert([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);
        $query = DB::table('casts')->where('id', $id)->update([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        return redirect('/cast');
    }

    public function destroy($id)
    {
        $cast = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
