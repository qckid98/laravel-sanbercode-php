<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerPage()
    {
        return view('form');
    }
    public function send(Request $request) {
        // dd($request->all());
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        return view ('welcome', compact('first_name', 'last_name'));
    }
}
