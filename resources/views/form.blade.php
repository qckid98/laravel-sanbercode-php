@extends('layout.master')
@section('title')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
  @csrf
  <label for="">First name:</label>
  <br />
  <br />
  <input type="text" name="first_name" />
  <br />
  <br />
  <label for="">Last name:</label>
  <br />
  <br />
  <input type="text" name="last_name" />
  <br />
  <br />
  <label for="">Gender:</label>
  <br />
  <br />
  <input type="radio" name="gender_radio" value="Male" id="gender_1" /> Male
  <br />
  <input type="radio" name="gender_radio" value="Female" id="gender_2" />
  Female
  <br />
  <input type="radio" name="gender_radio" value="Other" id="gender_3" />
  Other
  <br />
  <br />
  <label>Nationality</label>
  <br />
  <br />
  <select name="nationality">
    <option value="Indonesia">Indonesian</option>
    <option value="nat-other">Other</option>
  </select>
  <br /><br />
  <label>Language Spoken:</label>
  <br />
  <br />
  <input type="checkbox" name="language" value="lang-id" /> Bahasa Indonesia
  <br />
  <input type="checkbox" name="language" value="lang-en" /> English <br />
  <input type="checkbox" name="language" value="lang-other" /> Other <br />
  <br />
  <label>Bio:</label>
  <br />
  <br />
  <textarea name="bio" id="bio_text" cols="30" rows="10"></textarea>
  <br />
  <button type="submit">Sign Up</button>
</form>
@endsection
    
