@extends('layout.master')
@section ('title')
Show Cast {{$cast->id}}
@endsection
@section('content')
<h4>Nama: {{$cast->nama}}</h4>
<p>Umur: {{$cast->umur}}</p>
<p>Bio: {{$cast->bio}}</p>
<a href="{{route('cast.edit', ['id' => $cast->id])}}" class="btn btn-primary">Edit</a>
<form action="{{route('cast.destroy', ['id' => $cast->id])}}" method="POST">
@csrf
@method('DELETE')
<input type="submit" class="btn btn-danger my-1" value="Delete">
@endsection